import QtQuick 1.1
import com.ingima.qmlplot 1.0

Item {

    //anchors.fill: parent
    Item {
        id: conteneur
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: switchToRTButton.left
        anchors.rightMargin: 4

        anchors.bottom : fpsinfos.top
        anchors.bottomMargin: 10

        QmlPlot {
            id: plot
            anchors.fill: parent
            color: "red"
        }
//        MouseArea {
//            anchors.fill: parent
//        }
    }

    Rectangle {
        id: switchToRTButton
        anchors.top : parent.top
        anchors.topMargin: 20
        anchors.right : parent.right
        anchors.rightMargin: 7
        height : 80
        width : 100

        color: "green"
        Text {
            anchors.fill: parent
            text: "Passer en Courbe Suivie"
        }

        MouseArea {
            anchors.fill: parent
            property bool rtOn: false
            onClicked: {
                if(rtOn)
                    rtOn = false
                else
                    rtOn = true
                plot.switchToRT(rtOn)
            }
        }
    }

    Rectangle {
        id: addGraphButton
        anchors.left: conteneur.right
        anchors.leftMargin: 4
        anchors.top : switchToRTButton.bottom
        anchors.topMargin: 10
        anchors.right : parent.right
        anchors.rightMargin: 7
        height : 80

        color: "blue"
        Text {
            anchors.fill: parent
            text: "Ajouter une voie"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: plot.addChannel()
        }
    }

    Text {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        height: 50
        anchors.horizontalCenter: parent.horizontalCenter
        id: fpsinfos
        text: plot.infos
    }
}
