@echo off
rem
rem This file is generated
rem

set QTDIR=C:\Qt\4.8.4
set QMAKESPEC=win32-msvc2010
set "QTCREATOR=C:\Qt\qtcreator-2.8.0"
set "PATH=%QTDIR%\bin;%QTDIR%\lib;%QTCREATOR%\bin;%PATH%"
rem ****************************************************************************
rem Set SDCN_PROJECT
set "ENN_PROJECT=%CD%"

set "ENN_IMPORTS=%ENN_PROJECT%\imports"
set "QML_IMPORT_PATH=%ENN_IMPORTS%"
cmd