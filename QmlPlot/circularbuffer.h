#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <vector>
#include <numeric>

template < typename T >
class CircularBuffer
{
public:
    CircularBuffer(int _size):
        idx(0),
        size(_size)
    {
        vec = std::vector<T>(_size);
    }

    void push_back(T& elt)
    {
        vec[ idx++ % size ] = elt;
    }


    int average()
    {
        return std::accumulate(vec.begin(), vec.end(), 0) / vec.size();
    }


private:
    int idx;
    int size;
    std::vector<T> vec;

};



#endif // CIRCULARBUFFER_H
