#include "realtimecustomplot.h"
#include <QDebug>

RealTimeCustomPlot::RealTimeCustomPlot(QWidget *parent) :
    QCustomPlot(parent)
{
}

void RealTimeCustomPlot::setup()
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
    QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif

    // include this section to fully disable antialiasing for higher performance:
//    setNotAntialiasedElements(QCP::aeAll);
//    QFont font;
//    font.setStyleStrategy(QFont::NoAntialias);
//    xAxis->setTickLabelFont(font);
//    yAxis->setTickLabelFont(font);
//    legend->setFont(font);


    yAxis->setTickLabelType(QCPAxis::ltDateTime);
    yAxis->setDateTimeFormat("hh:mm:ss");
    yAxis->setAutoTickStep(true);
    axisRect()->setupFullAxesBox();

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(xAxis, SIGNAL(rangeChanged(QCPRange)), xAxis2, SLOT(setRange(QCPRange)));
    connect(yAxis, SIGNAL(rangeChanged(QCPRange)), yAxis2, SLOT(setRange(QCPRange)));

    setInteractions(QCP::iRangeDrag | QCP::iRangeZoom );
    axisRect()->setRangeZoom(Qt::Vertical);

    lastPointKey = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    //yAxis->setRangeReversed(true);
    //yAxis->setRange(lastPointKey+0.1, 16, Qt::AlignBottom);
    // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
    connect(&dataTimer, SIGNAL(timeout()), this, SLOT(feedData()));
    dataTimer.start(10); // Interval 0 means to refresh as fast as possible
}

/**
 * @brief Cette fonction est appeler periodiquement par un timer et ajoute des données aux QCPDataMap de
 *      tous les channels (graph) actif. Elle permet d'émuler le comportement qu'on aurait avec des
 *      Importeurs ALICES en étant connecté au PROCEDE en mode EXERCICE / CONTINU.
 */
void RealTimeCustomPlot::feedData()
{
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    if (key-lastPointKey > 0.1) // at most add point every 10 ms
    {
        for (int i = 0; i < graphCount(); i++) {

            double value = qSin(((i+1)*key +(i%3)*3.14)*0.0005) + i; //sin(key*1.6+cos(key*1.7)*2)*10 + sin(key*1.2+0.56)*20 + 26;           
            graph(i)->addData(key, value);
        }
        lastPointKey = key;
    }
}


void RealTimeCustomPlot::refreshRT()
{
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;

    // make key axis range scroll with the data (at a constant range size of 8):
    yAxis->setRange(key, 16, Qt::AlignRight);
    xAxis->rescale();
    replot();

    ++frameCount;
    if (key-lastFpsKey > 2) // average fps over 2 seconds
    {
        emit publishFps(frameCount/2, graph(0)->data()->count());

        lastFpsKey = key;
        frameCount = 0;
    }

}

void RealTimeCustomPlot::addChannel() {

    if (graphCount() == 0) {
        setup();
    }

    addGraph(yAxis, xAxis); // blue line
    int mod = graphCount();
    graph()->setPen(QPen(QColor(85*(mod%3), 85*((mod+1)%3), 85*((mod+2)%3))));

    const int n = 2800;
    double start = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0 - n*0.1;

    QCPData newData;
    for (int i=1; i < n; ++i)
    {
      newData.key = start + 0.1*i;
      newData.value = qSin(newData.key/1000) + mod;
      graph()->data()->insertMulti(newData.key, newData);
    }
}

void RealTimeCustomPlot::switchToRT(bool on)
{
    if (on) {
        connect(&dataTimer, SIGNAL(timeout()), this, SLOT(refreshRT()));
    } else {
        disconnect(&dataTimer, SIGNAL(timeout()), this, SLOT(refreshRT()));
    }
}
