#include "qmlplot.h"
#include <QVector>
#include <qdeclarative.h>

QmlPlot::QmlPlot(QDeclarativeItem *parent):
    QGraphicsProxyWidget(parent),
    m_infos("")
{
    customPlot = new RealTimeCustomPlot();
    setWidget(customPlot);
    connect(customPlot, SIGNAL(publishFps(int,int)), this, SLOT(receiveStats(int,int)));
    setVisible(true);
}

QmlPlot::~QmlPlot()
{
}

void QmlPlot::setup()
{
    customPlot->setup();
    update();
}

void QmlPlot::addChannel()
{
    customPlot->addChannel();
}

void QmlPlot::switchToRT(bool on)
{
    customPlot->switchToRT(on);
}

QColor QmlPlot::color() const
{
    return m_color;
}

QString QmlPlot::infos() const
{
    return m_infos;
}


void QmlPlot::setcolor(QColor arg)
{
    if (m_color != arg) {
        m_color = arg;
        emit colorChanged(arg);
    }
}

void QmlPlot::receiveStats(int afps, int apoints)
{
    emit setInfos(QString("%1 FPS, Total Data points: %2").arg(afps).arg(apoints));
}

void QmlPlot::setInfos(QString arg)
{
    if (m_infos != arg) {
        m_infos = arg;
        emit infosChanged(arg);
    }
}

