TEMPLATE = lib
QT += declarative
CONFIG += qt plugin

TARGET = QmlPlot

TARGET = $$qtLibraryTarget($$TARGET)
uri = com.ingima.qmlplot
TARGETPATH = $$replace(uri,\\.,$$QMAKE_DIR_SEP)


# Input
SOURCES += \
    qmlplot_plugin.cpp \
    qmlplot.cpp \
    qcustomplot.cpp \
    realtimecustomplot.cpp

HEADERS += \
    qmlplot_plugin.h \
    qmlplot.h \
    qcustomplot.h \
    realtimecustomplot.h \
    circularbuffer.h

OTHER_FILES = qmldir

DESTDIR = $$OUT_PWD

target.path = $$(ENN_IMPORTS)/$$TARGETPATH

qmldir.files += $$PWD/qmldir
qmldir.path += $$(ENN_IMPORTS)/$$TARGETPATH

INSTALLS += target qmldir

