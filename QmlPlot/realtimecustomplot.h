#ifndef REALTIMECUSTOMPLOT_H
#define REALTIMECUSTOMPLOT_H

#include "qcustomplot.h"
#include <QTimer>

class RealTimeCustomPlot : public QCustomPlot
{
    Q_OBJECT
public:
    explicit RealTimeCustomPlot(QWidget *parent = 0);
    void setup();

    void addChannel();
    void switchToRT(bool on);

signals:
    void publishFps(int fps, int points);

private slots:
    void feedData();
    void refreshRT();


private:
    QTimer dataTimer;

    // calculate frames per second:
    double lastFpsKey;
    int frameCount;
    double lastPointKey;

};

#endif // REALTIMECUSTOMPLOT_H
