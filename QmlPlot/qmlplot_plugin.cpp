#include "qmlplot_plugin.h"
#include "qmlplot.h"

#include <qdeclarative.h>

void QmlPlotPlugin::registerTypes(const char *uri)
{
    // @uri com.ingima.qmlplot
    qmlRegisterType<QmlPlot>(uri, 1, 0, "QmlPlot");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(QmlPlot, QmlPlotPlugin)
#endif

