#ifndef QMLPLOT_H
#define QMLPLOT_H

#include <QGraphicsProxyWidget>
#include <QDeclarativeItem>
#include <QPainter>
#include "realtimecustomplot.h"

class QmlPlot : public QGraphicsProxyWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(QmlPlot)
    Q_PROPERTY(QColor color READ color WRITE setcolor NOTIFY colorChanged)
    Q_PROPERTY(QString infos READ infos WRITE setInfos NOTIFY infosChanged)

public:
    QmlPlot(QDeclarativeItem *parent = 0);
    ~QmlPlot();

    Q_INVOKABLE void setup();
    Q_INVOKABLE void addChannel();
    Q_INVOKABLE void switchToRT(bool on);

    QColor color() const;

    QString infos() const;

public slots:
    void setcolor(QColor arg);
    void receiveStats(int afps, int apoints);

    Q_INVOKABLE void setInfos(QString arg);

signals:
    void colorChanged(QColor arg);

    void infosChanged(QString arg);

private:
    QColor m_color;
    RealTimeCustomPlot *customPlot;

    QString m_infos;
};

QML_DECLARE_TYPE(QmlPlot)

#endif // QMLPLOT_H

